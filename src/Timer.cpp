/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Timer.hpp>

Timer::Timer()
{
  this->startPoint = std::chrono::high_resolution_clock::now();
}
void Timer::start()
{
  this->startPoint = std::chrono::high_resolution_clock::now();
}
void Timer::end()
{
  this->duration = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-this->startPoint);
}
double Timer::getTime()
{
  return this->duration.count();
}
double Timer::getRate()
{
  return 1/this->getTime();
}
