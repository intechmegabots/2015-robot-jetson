/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CamCatcher.hpp"
#include <thread>
#include <iostream>
#include <Qs.hpp>

Lock::Lock(std::mutex &m)
{
  this->m=&m;
  m.lock();
}
Lock::~Lock()
{
  m->unlock();
}

CamCatcher::CamCatcher(const std::string &name) : cam(name)
{
  this->_h=getH();
  this->_w=getW();
  isLocal=false;
}
CamCatcher::CamCatcher(int name) : cam(name) {
  isLocal=true;
}

void CamCatcher::printInfo(std::string str)
{
  std::cerr << Qs(
  " -- Camera %1 Info -- \n"
  "======================\n"
  "Width  | %2 \n"
  "Height | %3 \n"
  "Frames | %4 \n").arg(str).arg(cam.get(CV_CAP_PROP_FRAME_WIDTH)).arg(cam.get(CV_CAP_PROP_FRAME_HEIGHT)).arg(cam.get(CV_CAP_PROP_FRAME_COUNT)) << std::flush;
}


void CamCatcher::start()
{
  if(!cam.isOpened())
  {
    camFast = cv::Mat(_h,_w,CV_8UC3,cv::Scalar(50,50,50));
    cv::putText(camFast,"Cannot open camera",cv::Point(_w/4,_h/2),CV_FONT_HERSHEY_SIMPLEX,1,(255,255,255),1);
    return;
  }
  camFast = cv::Mat(getH(),getW(),CV_8UC3,cv::Scalar(0,0,0));
  this->run = true;
  std::thread([=](){
    while(this->run)
    {
      cam.read(camCap);
      matLock.lock();
      camCap.copyTo(camFast);
      matLock.unlock();
      timerLock.lock();
      timer.end();
      timer.start();
      timerLock.unlock();
    }
  }).detach();
}

void CamCatcher::setResolution(int w, int h)
{
  _w=w;
  _h=h;
  if(!isLocal) return;
  cam.set(CV_CAP_PROP_FRAME_HEIGHT,h);
  cam.set(CV_CAP_PROP_FRAME_WIDTH,w);
}
int CamCatcher::getH()
{
  return cam.get(CV_CAP_PROP_FRAME_HEIGHT);
}
int CamCatcher::getW()
{
  return cam.get(CV_CAP_PROP_FRAME_WIDTH);
}

double CamCatcher::getFramerate()
{
  timerLock.lock();
  double t = timer.getRate();
  timerLock.unlock();
  return t;
}

void CamCatcher::copy(cv::Mat& m)
{
  matLock.lock();
  //std::cerr << m.size() << " " << m.type() << std::endl;
  //std::cerr << camFast.size() << " " << camFast.type() << std::endl;
  camFast.copyTo(m);
  matLock.unlock();
}

