#ifndef BOXFINDER_H
#define BOXFINDER_H

#include <CamCatcher.hpp>
#include <mutex>

class BoxFinder
{
public:
  BoxFinder(CamCatcher*);
  void start();
  void stop();
  ~BoxFinder();
  std::mutex dataLock;
  float area;
  float x;
  float y;
  int canFind;
private:
  std::mutex runningLock;
  CamCatcher* cam;
  bool running;
};

#endif // BOXFINDER_H
