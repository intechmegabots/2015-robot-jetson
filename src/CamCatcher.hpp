/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAM_CATCHER_HPP_GUARD
#define CAM_CATCHER_HPP_GUARD

#include <opencv2/highgui/highgui.hpp>
#include <string.h>
#include <atomic>
#include <mutex>
#include <Timer.hpp>
#include <chrono>

class CamCatcher {
public:
  CamCatcher(int name);
  CamCatcher(const std::string &name);
  void start();
  void setResolution(int w, int h);
  void printInfo(std::string str);
  int getH();
  int getW();
  double getFramerate();
  void copy(cv::Mat& m);
//private:
  std::atomic<bool> run;
  std::mutex matLock;
  std::mutex timerLock;
  cv::VideoCapture cam;
  cv::Mat camCap;
  cv::Mat camFast;
  Timer timer;
  int _w;
  int _h;
  bool isLocal;
};

class Lock
{
public:
  Lock(std::mutex& m);
  ~Lock();
private:
  std::mutex *m;
};

#endif //CAM_CATCHER_HPP_GUARD
