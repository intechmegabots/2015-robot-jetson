/*
Copyright 2015 Abex

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Qs.hpp"
#include <vector>
#include <string>

Qs& Qs::arg(int arg)
{
  format(std::to_string(arg));
  return *this;
}
Qs& Qs::arg(float arg)
{
  format(std::to_string(arg));
  return *this;
}
Qs& Qs::arg(double arg)
{
  format(std::to_string(arg));
  return *this;
}
Qs& Qs::arg(std::string arg)
{
  format(arg);
  return *this;
}
std::string& Qs::str()
{
  return *this;
}
void Qs::format(std::string arg)
{
  if(!hasOrig)
  {
    this->original = *this;
    this->strs = new std::vector<std::string>();
    this->hasOrig=true;
  }
  int ci=0;
  this->strs->emplace_back(arg);
  this->resize(this->size()+arg.size()-2);
  for(size_t i=0;i<this->original.size();i++)
  {
    if(original[i]=='%' && original.size()>(i+1))
    {
      if(original[i+1]=='%') (*this)[ci++] = '%';
      if(original[i+1]>'1' || original[i+1]<'9')
      {
        size_t in = original[++i]-'1';
        if(in<this->strs->size())
          for(size_t q=0;q<this->strs->at(in).size();q++)
          {
            (*this)[ci++] = this->strs->at(in)[q];
          }
      }
    }
    else
    {
      (*this)[ci++] = original[i];
    }
  }
}
