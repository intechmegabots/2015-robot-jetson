/*
Copyright 2015 Abex

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef QS_HPP
#define QS_HPP

#include <string>
#include <vector>

class Qs : public std::string
{
public:
  Qs& arg(std::string arg);
  Qs& arg(int arg);
  Qs& arg(float arg);
  Qs& arg(double arg);
  std::string& str();
  using std::string::string;
private:
  std::string original;
  bool hasOrig=false;
  std::vector<std::string> *strs;
  void format(std::string arg);
};

#endif // QS_HPP

