#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <Qs.hpp>
#include <VideoStreamer.h>
#include <Timer.hpp>
#include <CamCatcher.hpp>
#include <Networking.h>
#include <thread>

// DEBUGING NOTE --------------------------------------
// ALWAYS use std::cerr and NOT std::cout because stdout is
// used solely for rgb24 image data streaming to ffmpeg and
// ffserver.


#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef __USE_GNU
#define __USE_GNU
#endif

#include <execinfo.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <unistd.h>

/* This structure mirrors the one found in /usr/include/asm/ucontext.h */
typedef struct _sig_ucontext {
 unsigned long     uc_flags;
 struct ucontext   *uc_link;
 stack_t           uc_stack;
 struct sigcontext uc_mcontext;
 sigset_t          uc_sigmask;
} sig_ucontext_t;

void crit_err_hdlr(int sig_num, siginfo_t * info, void * ucontext)
{
 void *             array[50];
 void *             caller_address;
 char **            messages;
 int                size, i;
 sig_ucontext_t *   uc;

 uc = (sig_ucontext_t *)ucontext;

 /* Get the address at the time the signal was raised *//*
#if defined(__i386__) // gcc specific
 caller_address = (void *) uc->uc_mcontext.eip; // EIP: x86 specific
#elif defined(__x86_64__) // gcc specific
 caller_address = (void *) uc->uc_mcontext.rip; // RIP: x86_64 specific
#else
#error Unsupported architecture. // TODO: Add support for other arch.
#endif*/
 caller_address = (void*) uc->uc_mcontext.arm_pc;// cant remember the arm #define so fuck it

 fprintf(stderr, "signal %d (%s), address is %p from %p\n",
  sig_num, strsignal(sig_num), info->si_addr,
  (void *)caller_address);
 size = backtrace(array, 50);

 /* overwrite sigaction with caller's address */
 array[1] = caller_address;
 messages = backtrace_symbols(array, size);
 /* skip first stack frame (points here) */
 for (i = 1; i < size && messages != NULL; ++i)
 {
  fprintf(stderr, "[bt]: (%d) %s\n", i, messages[i]);
 }
 free(messages);
 exit(EXIT_FAILURE);
}


int w = 1350; // also in run & ffserver.conf
int h = 500;

void copyMat(cv::Mat &dest, cv::Mat &src,cv::Rect destROI, cv::Rect srcROI)
{
  src(srcROI).copyTo(dest(destROI));
}
void copyAndConvert(cv::Mat &dest, cv::Mat &src,cv::Rect destROI, cv::Rect srcROI)
{
  cv::cvtColor(src(srcROI),dest(destROI),CV_BGR2RGB);
}
void copyAndFlip(cv::Mat &dest, cv::Mat &src,cv::Rect destROI, cv::Rect srcROI)
{
  cv::flip(src(srcROI),dest(destROI),0);
}

void disp(cv::Mat &src, int x, int y, int w, int h, int dx, int dy, void(cpFunc)(cv::Mat &dest, cv::Mat &src,cv::Rect destROI, cv::Rect srcROI));
#define disp(a,b,c,d,e,f,g,h) _disp(a,b,c,d,e,f,g,h,outputFrame)

void _disp(cv::Mat &src, int x, int y, int w, int h, int dx, int dy,void(cpFunc)(cv::Mat &dest, cv::Mat &src,cv::Rect destROI, cv::Rect srcROI),cv::Mat &dest)
{
  if(w<=0) return;
  if(h<=0) return;
  if(dx>=dest.cols) return;
  if(dy>=dest.rows) return;
  if(x>=src.rows) return;
  if(y>=src.cols) return;
  if(x<0)x=0;
  if(y<0)y=0;
  if(dx<0)dx=0;
  if(dy<0)dy=0;
  if((x+w)>=src.cols)w=src.cols-x;
  if((dx+w)>=dest.cols)w=dest.cols-dx;
  if((y+h)>src.cols)h=src.rows-y;
  if((dy+h)>dest.cols)h=dest.rows-dy;
  //std::cerr << x << " " << y << " " << w << " " << h << " " << dx << " " << dy << std::endl;
  cv::Rect srcROI(x,y,w,h);
  cv::Rect destROI(dx,dy,w,h);
  cpFunc(dest,src,destROI,srcROI);
}
//http://extcam-14.se.axis.com/axis-cgi/mjpg/video.cgi?camera=1&resolution=160x120&compression=100
// that is beautiful
std::string CameraURL = "http://%3/axis-cgi/mjpg/video.cgi?camera=1&resolution=%1&compression=%2";

const int camBot= 0;
const int camTop= 1;
const int camClaw=2;

int main()
{
  struct sigaction sigact;

  sigact.sa_sigaction = crit_err_hdlr;
  sigact.sa_flags = SA_RESTART | SA_SIGINFO;

  if (sigaction(SIGSEGV, &sigact, (struct sigaction *)NULL) != 0)
   fprintf(stderr, "error setting signal handler for %d (%s)\n", SIGSEGV, strsignal(SIGSEGV));
  if (sigaction(SIGABRT, &sigact, (struct sigaction *)NULL) != 0)
   fprintf(stderr, "error setting signal handler for %d (%s)\n", SIGABRT, strsignal(SIGABRT));

  VideoStreamer vs(w,h);

  cv::Mat preroll(h,w,CV_8UC3,cv::Scalar(30,30,30));
  cv::putText(preroll,"Starting",cv::Point(w/4,h/2),CV_FONT_HERSHEY_SIMPLEX,1,(255,255,255),1);
  vs.writeFrame(preroll);

  CamCatcher BotCam(0); // usb - @ bottom of robot (0)
  BotCam.setResolution(960,544);
  BotCam.printInfo("BottomCamera");
  BotCam.start();

  CamCatcher TopCam("http://10.29.93.11/mjpg/video.mjpg"); //(1)
  TopCam.setResolution(640,480);
  TopCam.printInfo("TopCamera");
  TopCam.start();

  BoxFinder bf(&BotCam);

  Networking rio(8000,&bf); // cannot be sent over wireless

  std::cerr << "Ready!" << std::endl;
  while(true)
  {
    cv::Mat outputFrame = cv::Mat(h,w,CV_8UC3,cv::Scalar(0,0,0));
    {
      Lock(BotCam.matLock);
      disp(BotCam.camFast,95,0,770,500,0,0,copyAndConvert);
    }
    {
      Lock(TopCam.matLock);
      disp(TopCam.camFast,30,0,580,480,770,0,copyAndConvert);
    }
    vs.writeFrame(outputFrame);
    std::this_thread::sleep_for(std::chrono::seconds(1/15));
  }
  return 0;

}
