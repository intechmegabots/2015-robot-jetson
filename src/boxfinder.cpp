#include "boxfinder.h"
#include "CamCatcher.hpp"
#include <opencv2/imgproc/imgproc.hpp>
#include <thread>

BoxFinder::BoxFinder(CamCatcher *cam)
{
  canFind=false;
  area=0;
  x=0;
  y=0;
  this->cam=cam;
}

void BoxFinder::start()
{
  {
    Lock(this->runningLock);
    if(this->running) return;
  }
  std::thread([=](){
    int cleanSize = 5;
    //https://jsfiddle.net/8cghfsok/9/
    int lowH = 23;
    int lowS = 100;
    int lowV = 100;

    int hiH = 37;
    int hiS = 255;
    int hiV = 255;
    while(true)
    {
      {
        Lock(this->runningLock);
        if(!this->running) return;
      }

      cv::Mat frame;
      {
        Lock(cam->matLock);
        cv::cvtColor(cam->camFast,frame,CV_BGR2HSV);
      }
      cv::inRange(frame,cv::Scalar(lowH,lowS,lowV),cv::Scalar(hiH,hiS,hiV),frame);

      //clean up small areas of light / dark
      cv::erode(frame, frame, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(cleanSize, cleanSize)));
      cv::dilate(frame, frame, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(cleanSize, cleanSize)));
      cv::dilate(frame, frame, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(cleanSize, cleanSize)));
      cv::erode(frame, frame, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(cleanSize, cleanSize)));

      cv::Moments moms = cv::moments(frame);
      double psArea = moms.m00;

      {
        Lock(this->dataLock);
        this->area = psArea/(frame.cols*frame.rows*255);
        if(area>.1)
        {
          this->canFind=true;
          this->x=(moms.m10/area)/frame.cols;
          this->y=(moms.m01/area)/frame.rows;
        }
        else
        {
          this->canFind=false;
        }
      }

    }
  }).detach();
}

void BoxFinder::stop()
{
  Lock(this->runningLock);
  this->running=false;
}

BoxFinder::~BoxFinder()
{

}

