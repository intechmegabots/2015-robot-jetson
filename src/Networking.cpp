/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <thread>
#include <iostream>
#include <cstring>
#include <Networking.h>
#include <PracticalSocket.h>

bool beginsWith(char* str, const char* end)
{
  for(size_t i=0;;i++)
  {
    if(end[i]=='\0') return true;
    if(str[i]=='\0') return false;
    if(str[i]!=end[i]) return false;
  }
}

#define REPLY(str) sock->send( str ,strlen( str ));

size_t indexOf(char* a,char c)
{
  for(size_t i=0;;i++)
  {
    if(a[i]=='\0') return i;
    if(a[i]==c) return i;
  }
}

Networking::Networking(int port,BoxFinder* bf) // lazy mode -- I should be using protobuf or something...
{
  CamA=0;
  CamB=1;
  std::thread([=](){
    TCPServerSocket server(port);
    while(1)
    {
      TCPSocket *sock = server.accept();
      std::cerr <<sock->getForeignAddress() << " has connected" << std::endl;
      char buf[256];
      memset(buf,'\0',256);
      while(sock->recv(buf,255)>0) // want to have the \0
      {
        if(beginsWith(buf,"PING\n")){
          REPLY("ACK\n");
        }
        else if(beginsWith(buf,"MSG"))
        {
          size_t os = 4;

          Lock(this->msgLock);
          this->msgs.push_back(std::string(buf+4,indexOf(buf+4,'\n')));
          REPLY("ACK\n");
        }
        else if(beginsWith(buf,"RIODATA")){


        }
        else if(beginsWith(buf,"AUTOGET"))
        {
          //size_t os = strlen("AUTOGET")+1;
          size_t len = 25; //autodata + 3 floats + 1 int + \0
          char* data = new char[len];
          int i=0;
          data[i++]='A';
          data[i++]='U';
          data[i++]='T';
          data[i++]='O';
          data[i++]='D';
          data[i++]='A';
          data[i++]='T';
          data[i++]='A';
          {
            Lock(bf->dataLock);
            memcpy(data+8,&bf->area,4);
            memcpy(data+12,&bf->x,4);
            memcpy(data+16,&bf->y,4);
            memcpy(data+20,&bf->canFind,4);
          }
          data[24]='\0';
          sock->send(data,len);
          delete data;
        }
        else if(beginsWith(buf,"AUTON"))
        {
          size_t os = strlen("AUTON")+1;
          if(atoi(buf+os))
            bf->start();
          else
            bf->stop();
          REPLY("ACK\n");
        }
        else {
          REPLY("NACK\n");
        }
        memset(buf,'\0',256);
      }
      delete sock;
    }
  }).detach();
}



Networking::~Networking()
{


}

