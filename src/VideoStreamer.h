/*
Copyright 2015 Intech Megabots

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef VIDEOSTREAMER_H
#define VIDEOSTREAMER_H

#include <opencv2/highgui/highgui.hpp>

class VideoStreamer
{
public:
  VideoStreamer(int w, int h);
  void writeFrame(cv::Mat& frame);
  ~VideoStreamer();
};

#endif // VIDEOSTREAMER_H
