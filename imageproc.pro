QMAKE_CXXFLAGS += -std=c++11 -O2 -rdynamic -mapcs-frame -funwind-tables

QT -= core gui

LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -L/usr/local/lib

INCLUDEPATH += src

HEADERS += \
    src/Qs.hpp \
    src/CamCatcher.hpp \
    src/Timer.hpp \
    src/Networking.h \
    src/VideoStreamer.h \
    src/PracticalSocket.h \
    src/boxfinder.h

SOURCES += \
    src/CamCatcher.cpp \
    src/main.cpp \
    src/Qs.cpp \
    src/Timer.cpp \
    src/Networking.cpp \
    src/VideoStreamer.cpp \
    src/PracticalSocket.cpp \
    src/boxfinder.cpp

win32:INCLUDEPATH += D:\opencv\build\include
